import sys
import os
from PIL import Image

#1. from sys module - grap first and second argument (Pokedex, new) - adica se vor lua toate JPG-urile din Pokedex si se vor pune, convertite in PNG in new(folder)

# cu biblioteca sys.argvs - aducem folderele in aplicatia noastra (vechi si cel nou creat)

image_folder = sys.argv[1]
ouput_folder = sys.argv[2]


#2. check if new\ exsist, if not create

# print(os.path.exists(ouput_folder)) # verificam inainte sa creem if-ul daca exista folderul => False (adica nu exista)
if not os.path.exists(ouput_folder): # daca folderul nu exista creem unul(traducere)
    os.makedirs(ouput_folder)        # aici se creaza folderul new dar in acelasi timp aceasta linie nu permite crearea unui folder similar la o rulare ulterioara

#3. loop through Pokedex,

for filename in os.listdir(image_folder): # se foloseste os.listdir() functia pentru a bucla prin Pokedex folder
    img = Image.open(f'{image_folder}{filename}') # se deschide pentru lucru fiecare imagine (filename) din Pokedex
    clean_name = os.path.splitext(filename)[0] # foarte important!!! - se creaza o variabila noua pentru a splita numele fisierului de extensie intr-un tuplu
                                              # si apoi index 0 pentru a retine doar numele fisierului

# 4.convert JPG to PNG and save to the new folder

    img.save(f'{ouput_folder}{clean_name}.png', 'png')  # se salveaza fisierul in nfolderul nou creat (new), cu extensia data.
    print('all done!')  # punem un mesaj ca sa ne afiseze de fiecare data cand se salveaza fisierul, ca totul este ok.

print(image_folder, ouput_folder)

#pentru procesare video si machine learning ,,open CV'' - open CV @ CVPR 2019

