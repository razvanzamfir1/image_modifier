from PIL import Image, ImageFilter

# img = Image.open('./Pokedex/pikachu.jpg')
img = Image.open('./astro.jpg')
# print(img.size) # verificam prima data dimensiunea

# 1. varinata in care imaginea se deformeaza
# new_img = img.resize((400, 400))
# new_img.save('tumbnail.jpg') # pentru ca imaginea nu are aceiasi inaltime si latime se poate deforma la modificare

2. # varianta buna de modificare imagine- cea care nu deformeaza imaginea cu thumbnail
img.thumbnail((400, 400))
img.save('thumbnail.jpg')
# filtered_img = img.filter(ImageFilter.BLUR) # aplicare filtru pe imagine TODO de incercat mai multe filtre
# filtered_img.save('blur.png', 'png')  #sese schimba numele si converteste in png, pentru ca png suporta filtrele 'BLUR, SHARPEN, etc'
# filtered_img = img.convert('L') # L reprezinta grey stil alt format decat RGB. TODO de incercat mai multe conversii

# crooked = filtered_img.rotate(180) # atentie - trebuie creata o variabila in care sa prindem actiunea functiei rotate de ex.
# crooked.save('grey.png', 'png')

# resize = filtered_img.resize((300, 300)) # atentie - reseze acepta tuple, valorile trebuie trecute in tuple!!!
# resize.save('grey.png', 'png')

# box =(100, 100, 400, 400) # se alege cati pixeli sa fie taiati din poza
# region = filtered_img.crop(box)
# region.save('grey.png', 'png')


# filtered_img.save('grey.png', 'png') # salveaza imaginea in alb si negru
# filtered_img.show() # ne afiseaza imaginea pe ecran

# print(img.mode)


